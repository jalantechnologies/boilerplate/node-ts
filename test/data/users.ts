import { User } from '@typings';

const users: User[] = [
  {
    _id: '1',
    firstName: 'John',
    lastName: 'Doe',
    email: 'john.doe@gmail.com',
    password: '$2b$12$SuXnNuPdy9mbG7ysPxwvcOI.xjTt.lBccJkIif0bZ5LR3Q3ftF9Cu',
  },
  {
    _id: '2',
    firstName: 'Jane',
    lastName: 'Doe',
    email: 'jane.doe@gmail.com',
    password: '$2b$12$SuXnNuPdy9mbG7ysPxwvcOI.xjTt.lBccJkIif0bZ5LR3Q3ftF9Cu',
  },
];
export default users;
