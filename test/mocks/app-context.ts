import { Repositories } from '@storage';
import { AppContext } from '@typings';
import { MockLogger } from './mock-logger';
import { MockStore } from './mock-store';

const mockStore = new MockStore();
export const testAppContext: AppContext = {
  logger: new MockLogger(),
  userRepository: new Repositories.UserRepository(mockStore),
};
