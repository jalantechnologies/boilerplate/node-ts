import { BaseModel, DeleteResult, IDataStore, LooseObject, ModelFactory, User, QueryOptions } from '@typings';
import users from '../data/users';

export class MockStore implements IDataStore {
  private findItemsInArray<T extends BaseModel>(
    data?: LooseObject,
    modelFactory?: ModelFactory<T>,
  ): T[] {
    const result = this.getItems<T>(modelFactory).filter((item) => {
      for (const field in data) {
        if (JSON.stringify(data[field]) !== JSON.stringify((item as LooseObject)[field])) {
          return false;
        }
      }
      return true;
    });
    return result;
  }

  public getAll<T extends BaseModel>(
    data?: LooseObject,
    options?: QueryOptions,
    modelFactory?: ModelFactory<T>,
  ): Promise<T[]> {
    if (!data) {
      return Promise.resolve(this.getItems<T>(modelFactory));
    }
    const result = this.findItemsInArray(data, modelFactory);
    return Promise.resolve(result);
  }

  public findById<T extends BaseModel>(
    id: string,
    options?: QueryOptions,
    modelFactory?: ModelFactory<T>,
  ): Promise<T> {
    const result = this.getItems<T>(modelFactory).find(i => i._id === id);
    return Promise.resolve(result);
  }

  public findOne<T extends BaseModel>(
    data?: LooseObject,
    options?: QueryOptions,
    modelFactory?: ModelFactory<T>,
  ): Promise<T> {
    const result = this.findItemsInArray(data, modelFactory);
    return Promise.resolve(result[0]);
  }

  public save<T extends BaseModel>(
    entity: T,
    modelFactory?: ModelFactory<T>,
  ): Promise<T> {
    const result = {
      ...entity,
      _id: (this.getItems<T>(modelFactory).length + 1).toString(),
    };
    this.getItems<T>(modelFactory).push(result);
    return Promise.resolve(result);
  }

  public update<T extends BaseModel>(
    filter: LooseObject,
    dataToUpdate: LooseObject,
    modelFactory?: ModelFactory<T>,
  ): Promise<T> {
    const tempResult = this.findItemsInArray(filter, modelFactory);
    const result = { ...tempResult[0], ...dataToUpdate };
    return Promise.resolve(result);
  }

  public toObjectId(id: string): any {
    return id;
  }

  public count<T extends BaseModel>(
    data?: LooseObject,
    modelFactory?: ModelFactory<T>,
  ): Promise<number> {
    if (!data) {
      return Promise.resolve(this.getItems<T>(modelFactory).length);
    }
    const result = this.findItemsInArray(data, modelFactory);
    return Promise.resolve(result.length);
  }

  public saveMany<T extends BaseModel>(
    entities: T[],
    modelFactory?: ModelFactory<T>,
  ): Promise<T[]> {
    let newId = this.getItems<T>(modelFactory).length + 1;

    for (const item of entities) {
      const newItem = { ...item, _id: newId.toString() };
      this.getItems<T>(modelFactory).push(newItem);
      newId += 1;
    }
    return Promise.resolve(entities);
  }

  public deleteMany<T extends BaseModel>(
    filter: LooseObject,
    modelFactory?: ModelFactory<T>,
  ): Promise<DeleteResult> {
    let deletedCount: number = 0;
    let items = this.getItems<T>(modelFactory);

    items = items.filter((item) => {
      for (const field in filter) {
        if (JSON.stringify(filter[field]) === JSON.stringify((item as LooseObject)[field])) {
          deletedCount += 1;
          return false;
        }
      }
      return true;
    });
    return Promise.resolve({ deletedCount, success: true });
  }

  private getItems<T extends BaseModel>(modelFactory?: ModelFactory<T>): T[] {
    if (modelFactory.getType() === typeof User) {
      return users.map((u) => {
        return modelFactory.create(u);
      });
    }
    return [];
  }
}
