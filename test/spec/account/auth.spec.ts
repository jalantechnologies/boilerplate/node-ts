// tslint:disable-next-line: no-var-requires
require('module-alias/register');

import { App } from '@server';
import chai from 'chai';
import chaiHttp from 'chai-http';
import { Application } from 'express';
import { testAppContext } from '../../mocks/app-context';

chai.use(chaiHttp);
const expect = chai.expect;
let expressApp: Application;

before(() => {
  const app = new App(testAppContext);
  app.initializeMiddlewares();
  app.initializeControllers();
  app.initializeErrorHandling();
  expressApp = app.expressApp;
});

describe(' POST /account/register', () => {
  it('should create a new account', () => {
    return chai
      .request(expressApp)
      .post('/account/register')
      .send({
        firstName: 'Bruce',
        lastName: 'Wayne',
        email: 'test@test.com',
        password: 'password',
      })
      .then((res) => {
        expect(res).to.have.status(201);
        expect(res.body).to.have.property('firstName');
        expect(res.body).to.have.property('lastName');
        expect(res.body).to.have.property('email');
      });
  });

  it('should return a validation error for incorrect data', () => {
    return chai
      .request(expressApp)
      .post('/account/register')
      .send({
        firstName: 'John',
        lastName: 'Doe',
        email: 'invalid email address',
      })
      .then((res) => {
        expect(res).to.have.status(400);
      });
  });

  it('should return an error for duplicate email', () => {
    return chai
      .request(expressApp)
      .post('/account/register')
      .send({
        email: 'john.doe@gmail.com',
        firstName: 'John',
        lastName: 'Doe',
        password: 'password',
      })
      .then((res) => {
        expect(res).to.have.status(400);
      });
  });
});

describe(' POST /account/login', () => {
  it('should return access token', () => {
    return chai
      .request(expressApp)
      .post('/account/login')
      .send({ email: 'john.doe@gmail.com', password: 'password' })
      .then((res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('token');
      });
  });
});
