# boilerplate API

API for the application built using NodeJS and Typescript.

## Scripts

- `npm run build` - For building the app.
- `npm run test` - Run automated test cases via `mocha` and `chai`.
- `npm run start` - For starting the app.
- `npm run serve` - For building and starting the app with auto-reload on changes.
- `npm run lint` - To perform linting

## Overview
- Typescript support
 - Gulp integration to compile to ES5 for production mode
 - Auto reload during development whenever code, or config changes

- Environment specific configuation support.
The application use [config](https://www.npmjs.com/package/config) npm module. Please see it's documentation for more details.

- Localization support: Support for multiple languages based on Accept-Language header.
The application use `i18n` npm module. Please see it's documentation for more details.

- Linting support
```
npm run lint
```

### Architecture

- Storage abstraction:
Introduced the concept of repository and data store. Data Store represents a storage medium (JSON, MongoDB, SQL etc.). The application can instantiate one or more storage. A repository corresponds provides 1:1 operation to a domain model (such as User) by using underlying data store provided by the application.

- Dependency Injection:
All the controller gets `AppContext` that has all the dependencies such as repositories to read/write data, logger etc. This makes it easier to replace their implementation in future.