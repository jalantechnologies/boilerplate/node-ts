import { IDataStore, LooseObject, DeleteResult, ModelFactory, QueryOptions } from '@typings';

export class BaseRepository<T> {
  constructor(private store: IDataStore) { }

  public getAll(data?: LooseObject, options?: QueryOptions): Promise<T[]> {
    return this.store.getAll<T>(data, options, this.modelFactory());
  }

  public findById(id: string, options?: QueryOptions): Promise<T> {
    return this.store.findById<T>(id, options, this.modelFactory());
  }

  public findOne(data?: LooseObject, options?: QueryOptions): Promise<T> {
    return this.store.findOne<T>(data, options, this.modelFactory());
  }

  public save(entity: T): Promise<T> {
    return this.store.save<T>(entity, this.modelFactory());
  }

  public update(filter: LooseObject, dataToUpdate: LooseObject): Promise<T> {
    return this.store.update<T>(filter, dataToUpdate, this.modelFactory());
  }

  public saveMany(entities: T[]): Promise<T[]> {
    return this.store.saveMany<T>(entities, this.modelFactory());
  }

  public toObjectId(id: string): any {
    return this.store.toObjectId(id);
  }

  public count(data?: LooseObject): Promise<number> {
    return this.store.count(data, this.modelFactory());
  }

  public deleteMany(filter: LooseObject): Promise<DeleteResult> {
    return this.store.deleteMany(filter, this.modelFactory());
  }

  protected modelFactory(): ModelFactory<T> {
    throw new Error('Not Implemented');
  }
}
