import { IDataStore, ModelFactory, User } from '@typings';
import { BaseRepository } from './base-repository';

export class UserRepository extends BaseRepository<User> {
  constructor(dataStore: IDataStore) {
    super(dataStore);
  }

  protected modelFactory(): ModelFactory<User> {
    return {
      getType() {
        return typeof User;
      },
      create(json: any) {
        return new User(json);
      },
    };
  }
}
