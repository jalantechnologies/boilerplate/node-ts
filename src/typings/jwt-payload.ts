export type JwtPayload = {
  userId: string;
  email?: string;
  phone?: string;
};
