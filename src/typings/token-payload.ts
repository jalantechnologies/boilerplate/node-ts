/**
 * Custom type used by the auth middleware to pass data to the request by token signing
 * (jwt.sign) and token verification (jwt.verify).
 * @param {string} userId - id of the user
 * @param {Object} permissions - permissions of the user
 */
export type TokenPayload = {
  userId: string;
  userRole: string;
  permissions: {
    ladderManagement: boolean;
    staffManagement: boolean;
    groupManagement: boolean;
  };
};
