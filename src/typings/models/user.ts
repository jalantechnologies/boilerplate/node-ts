import { BaseModel } from './base-model';

export class User extends BaseModel {
  firstName: string;
  lastName: string;
  email?: string;
  password?: string;

  constructor(json?: any) {
    super(json);
    if (json) {
      this.firstName = json.firstName;
      this.lastName = json.lastName;
      this.email = json.email;
      this.password = json.password;
    }
  }
}
