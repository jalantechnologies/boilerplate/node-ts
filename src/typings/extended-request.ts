import { User, ValidationFailure } from '@typings';
import { Request } from 'express';

export type ExtendedRequest = Request & {
  duplicateFieldFailures?: ValidationFailure[];
  userId?: string;
  userEmail?: string;
  user?: User;
};
