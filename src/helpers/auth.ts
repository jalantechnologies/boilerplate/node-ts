import bcrypt from 'bcrypt';
import config from 'config';
import jsonwebtoken from 'jsonwebtoken';
import { JwtPayload } from '@typings';

export const comparePassword = (
  inputPassword: string,
  userPassword: string,
): Promise<boolean> => {
  return bcrypt.compare(inputPassword, userPassword);
};

export const createAccessToken = (paylaod: JwtPayload): string => {
  return jsonwebtoken.sign(paylaod, config.get('jwt.key'), { expiresIn: '1d' });
};

export const decodeAccessToken = (token: string): JwtPayload => {
  return jsonwebtoken.verify(token, config.get('jwt.key')) as JwtPayload;
};

export const encryptPassword = (password: string): Promise<string> => {
  return bcrypt.hash(password, 12);
};
