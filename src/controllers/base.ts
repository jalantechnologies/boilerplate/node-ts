import { AppContext, Logger } from '@typings';
import { Router } from 'express';

export class Controller {
  public basePath: string;
  public router: Router;
  protected appContext: AppContext;

  constructor(ctx: AppContext) {
    this.appContext = ctx;
  }

  protected getLogger(): Logger {
    return this.appContext.logger;
  }
}
