import { Controller } from '@controllers';
import { AuthHelper, Validation } from '@helpers';
import { AppContext, Errors, ExtendedRequest, User, ValidationFailure } from '@typings';
import { loginValidator, registerValidator } from '@validators';
import { NextFunction, Response, Router } from 'express';
import lodash from 'lodash';

export class UserController extends Controller {
  public basePath: string = '/account';
  public router: Router = Router();

  constructor(ctx: AppContext) {
    super(ctx);
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      `${this.basePath}/register`,
      registerValidator,
      this.duplicateUserValidation,
      this.registerUser,
    );
    this.router.post(`${this.basePath}/login`, loginValidator, this.loginUser);
  }

  private duplicateUserValidation = (
    req: ExtendedRequest,
    res: Response,
    next: NextFunction,
  ) => {
    const { email } = req.body;
    const conflictFailure: ValidationFailure = {
      field: 'email',
      message: res.__('DUPLICATE_FIELD_ERRORS.USER.EMAIL'),
    };
    req.duplicateFieldFailures = req.duplicateFieldFailures || [];

    this.appContext.userRepository
      .findOne({ email })
      .then((user) => {
        if (!lodash.isEmpty(user)) {
          req.duplicateFieldFailures.push(conflictFailure);
        }
        return next();
      })
      .catch(next);
  }

  private registerUser = (
    req: ExtendedRequest,
    res: Response,
    next: NextFunction,
  ) => {
    const failures: ValidationFailure[] = Validation.extractValidationErrors(req);
    const { email, firstName, lastName, password } = req.body;

    if (failures.length > 0) {
      const valError = new Errors.ValidationError(
        res.__('DEFAULT_ERRORS.VALIDATION_FAILED'),
        failures,
      );
      return next(valError);
    }

    AuthHelper.encryptPassword(password)
      .then((hashedPassword) => {
        const newUser: User = {
          firstName,
          lastName,
          email,
          password: hashedPassword,
        };
        return this.appContext.userRepository.save(newUser);
      })
      .then((user) => {
        res.status(201).json(user);
      })
      .catch(next);
  }

  private loginUser = (
    req: ExtendedRequest,
    res: Response,
    next: NextFunction,
  ) => {
    const failures: ValidationFailure[] = Validation.extractValidationErrors(req);
    const { email, password } = req.body;
    let userId: string;

    if (failures.length > 0) {
      const valError = new Errors.ValidationError(
        res.__('DEFAULT_ERRORS.VALIDATION_FAILED'),
        failures,
      );
      return next(valError);
    }

    this.appContext.userRepository
      .findOne({ email })
      .then((user) => {
        if (lodash.isEmpty(user)) {
          const authError = new Errors.AuthenticationError(
            res.__('DEFAULT_ERRORS.LOGIN_AUTHENTICATION_FAILED'),
          );
          throw authError;
        }
        userId = user._id;
        return AuthHelper.comparePassword(password, user.password);
      })
      .then((isPasswordValid) => {
        if (!isPasswordValid) {
          const authError = new Errors.AuthenticationError(
            res.__('DEFAULT_ERRORS.LOGIN_AUTHENTICATION_FAILED'),
          );
          throw authError;
        }
        const token = AuthHelper.createAccessToken({
          userId,
          email,
        });
        res.json({ userId, email, token });
      })
      .catch(next);
  }
}
