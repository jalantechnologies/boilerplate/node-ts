import { Controller } from '@controllers';
import { AppContext } from '@typings';
import { NextFunction, Request, Response, Router } from 'express';

export class InternalController extends Controller {
  public basePath: string = '/healthcheck';
  public router: Router = Router();

  constructor(ctx: AppContext) {
    super(ctx);
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.basePath, this.runHealthcheck);
  }

  private runHealthcheck = (
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    res.json({ message: res.__('MESSAGES.HEALTHCHECK') });
  }
}
