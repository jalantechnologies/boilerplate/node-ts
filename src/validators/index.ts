import loginValidator from './login-validator';
import registerValidator from './register-validator';

export { registerValidator, loginValidator };
