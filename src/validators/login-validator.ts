import { check, ValidationChain } from 'express-validator';

const loginValidator: ValidationChain[] = [
  check('email')
    .isEmail()
    .withMessage('VALIDATION_ERRORS.INVALID_EMAIL'),
  check('password', 'VALIDATION_ERRORS.PASSWORD_MISSING').notEmpty(),
];

export default loginValidator;
