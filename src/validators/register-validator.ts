import { check, ValidationChain } from 'express-validator';

const registerValidator: ValidationChain[] = [
  check('firstName', 'VALIDATION_ERRORS.INVALID_FIRSTNAME')
    .notEmpty(),
  check('lastName', 'VALIDATION_ERRORS.INVALID_LASTNAME')
    .notEmpty(),
  check('email')
    .isEmail()
    .withMessage('VALIDATION_ERRORS.INVALID_EMAIL'),
  check('password', 'VALIDATION_ERRORS.PASSWORD_MISSING').notEmpty(),
];

export default registerValidator;
